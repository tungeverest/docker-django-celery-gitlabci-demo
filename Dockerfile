FROM python:3.9
# pull official base image

# set environment variables
ENV APP_ROOT /app
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

## create user for the Django project
# RUN useradd -ms /bin/bash myproject

# RUN addgroup -S $APP_USER && adduser -S $APP_USER -G $APP_USER

# install psycopg2 dependencies
# RUN apk add --update --no-cache --virtual .tmp-build-deps \
# 	gcc libc-dev linux-headers postgresql-dev python3-dev \
# 	musl-dev zlib zlib-dev
RUN apt-get update \
 && apt-get install -y --force-yes \
 nano python-pip gettext chrpath libssl-dev libxft-dev \
 libfreetype6 libfreetype6-dev  libfontconfig1 libfontconfig1-dev\
  && rm -rf /var/lib/apt/lists/*

RUN mkdir -p ${APP_ROOT}
RUN mkdir -p ${APP_ROOT}/static
RUN mkdir -p ${APP_ROOT}/media

# set work directory
WORKDIR ${APP_ROOT}
# expose the port 8000
EXPOSE 8000
# install dependencies
RUN pip install --upgrade pip setuptools

# [Security] Limit the scope of user who run the docker image
# RUN adduser -D user

# USER user

# install requirements
COPY ./requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

# Remove dependencies
# RUN apk del .tmp-build-deps

#copy entrypoint.sh
# COPY ./entrypoint.sh .
# RUN chmod a+x *.sh
# copy project

# COPY . .
# RUN chmod a+x entrypoint.sh
# CMD [ "./entry_point.sh" ]

# run entrypoint.sh
# ENTRYPOINT ["./entrypoint.sh"]
# CMD ["/bin/bash", "/app/entrypoint.sh"]

# define the default command to run when starting the container
# CMD ["gunicorn", "--chdir", "hello", "--bind", ":8000", "hello.wsgi:application"] 