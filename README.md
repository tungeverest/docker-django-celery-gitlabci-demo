# Descripton create this my Project & Run Local
mkdir .env  
cd ~/.env  
python3 -m venv django-runner-demo

create folder your project  
cd folder your project name  

source /home/user_name/.env/django-runner-demo/bin/activate

# Create new project
django-admin startproject projects .

django-admin.py startproject myproject ~/myproject
 
At this point, your project directory (~/myproject in our case) 

# Create app
cd projects
python manage.py startapp apps


# Create celery.py in projects folder

# Create task.py in apps folder

# Settings
INSTALLED_APPS += [
    'django_celery_beat',
    'django_celery_results',
]

CELERY_RESULT_BACKEND = "django-db"

python manage.py makemigrations
python manage.py migrate

manage.py createsuperuser new_user

We can collect all of the static content into the directory location we configured by typing:

python manage.py collectstatic

sudo ufw allow 8000

python manage.py runserver 0.0.0.0:8000
 
# Starting The Worker Process
celery -A projects worker -l --loglevel=INFO


# Run Scheduled Tasks With Celery Beat:
celery -A projects woker --beat -l info -S django

# Unicorn
gunicorn --bind 0.0.0.0:8000 projects.wsgi

## Create a Gunicorn systemd Service File

sudo nano /etc/systemd/system/gunicorn.service

==>>>/etc/systemd/system/gunicorn.service  
[Unit]  
Description=gunicorn daemon    
After=network.target  


sudo systemctl start gunicorn  
sudo systemctl enable gunicorn  
sudo systemctl status gunicorn  


# Configure Nginx to Proxy Pass to Gunicorn

sudo nano /etc/nginx/sites-available/myproject

```nginx
server {
    listen 80;
    server_name server_domain_or_IP;

    location = /favicon.ico { access_log off; log_not_found off; }
    location /static/ {
        root /home/sammy/myproject;
    }

    location / {
        include proxy_params;
        proxy_pass http://unix:/home/sammy/myproject/myproject.sock;
    }
}
```

sudo ln -s /etc/nginx/sites-available/myproject /etc/nginx/sites-enabled

# Test your Nginx configuration for syntax errors by typing:

sudo nginx -t

If no errors are reported, go ahead and restart Nginx by typing:

sudo systemctl restart nginx
 
Finally, we need to open up our firewall to normal traffic on port 80. Since we no longer need access to the development server, we can remove the rule to open port 8000 as well:  

sudo ufw delete allow 8000
sudo ufw allow 'Nginx Full'

#  Build from dockerfile and push Registry Example
docker build --tag python-app .  
docker run --publish 8000:8000 python-app