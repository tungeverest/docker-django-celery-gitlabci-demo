import time
from typing import Any, Optional
from django.db import connections
from django.db.utils import OperationalError
from django.core.management import BaseCommand

class Command(BaseCommand):
    """Django command to pause until db is available"""
    
    def handle(self, *args: Any, **options: Any) -> Optional[str]:
        
        self.stdout.write('waiting for database setup...')

        db_conn = None
        while not db_conn:
            try:
                db_conn = connections['default']
            except OperationalError:
                self.stdout.write('Database not available, please wait second...')
                time.sleep(0.5)
        self.stdout.write(self.style.SUCCESS('Database is available!!!'))