from __future__ import absolute_import, unicode_literals
from datetime import time
import os
from celery import Celery
from time import sleep

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'projects.settings')

# Get the base REDIS URL, default to redis' default
# BASE_BROKER_URL = os.environ.get('CELERY_BROKER_URL', 'redis://localhost:6379')
# BASE_BACKEND_URL = os.environ.get('CELERY_BACKEND_URL', 'redis://localhost:6379')
# app = Celery('projects', broker='BASE_BROKER_URL', backend='BASE_BACKEND_URL')

app = Celery('projects')

# Using a string here means the worker don't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# The app.autodiscover_tasks() configuration from above will automatically find these tasks.
# Load task modules from all registered Django app configs.
app.autodiscover_tasks()

# app.conf.broker_url = BASE_BROKER_URL

# this allows you to schedule items in the Django admin.
# app.conf.beat_scheduler = 'django_celery_beat.schedulers.DatabaseScheduler'

#debug test
@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
    
@app.task
def say_hello(name: str):
    sleep(2) 
    return f"Hello {name}"